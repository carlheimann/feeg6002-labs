#include <stdio.h>
#include <math.h>

#define XMIN 1.0
#define XMAX 10.0
#define N 10

int main(void){
    int i;
    double x, fx, step;
    step = (XMAX - XMIN)/(N-1.);
    for (i = 0; i < N; i++){
        x = XMIN + (i * step);
        fx = sin(x);
        printf("%f %f\n", x, fx);
    }
    return 0;
}
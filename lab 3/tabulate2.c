#include <stdio.h>
#include <math.h>

#define XMIN 1.0
#define XMAX 10.0
#define N 10

int main(void){
    int i;
    double x, sinx, cosx, step;
    step = (XMAX - XMIN)/(N-1.);
    for (i = 0; i < N; i++){
        x = XMIN + (i * step);
        sinx = sin(x);
        cosx = cos(x);
        printf("%f %f %f\n", x, sinx, cosx);
    }
    return 0;
}
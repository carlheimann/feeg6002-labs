#include <stdio.h>

int main(void){
    float BORROWED, RATE, debt, interest, totalInterest, interestFraction;
    int MONTHS, month;
    
    BORROWED = 1000;
    RATE = 0.03;
    MONTHS = 24;
    
    debt = BORROWED;
    totalInterest = 0;
    
    for (month = 1; month <= MONTHS; month++){
        interest = RATE * debt;
        debt += interest;
        totalInterest += interest;
        interestFraction = 100 * (totalInterest / BORROWED); /* Converted to percentage */
        printf("month %2d: debt=%7.2f, interest=%.2f, total_interest=%7.2f, frac=%6.2f%%\n", month, debt, interest, totalInterest, interestFraction);
    }
    
    /*month 2: debt=1060.90, interest=30.90, total_interest= 60.90, frac= 6.09%*/
    /*month 24: debt=2032.79, interest=59.21, total_interest=1032.79, frac=103.28%*/
    
    return 0;
}